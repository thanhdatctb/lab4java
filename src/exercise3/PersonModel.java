/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise3;

import java.util.ArrayList;

/**
 *
 * @author thanh
 */
public class PersonModel<T> {
    public ArrayList<T> li = new ArrayList<T>();
    public void addObject(T object){
        li.add(object);
    }
    public void display(){
        for(T e: li){
            System.out.println(e);
        }
    }
}
