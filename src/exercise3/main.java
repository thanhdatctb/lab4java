/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise3;

/**
 *
 * @author thanh
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        PersonModel<Student> stds = new PersonModel<>();
        stds.addObject(new Student(1, "Nguyễn Văn A", 12));
        stds.addObject(new Student(2, "Trần Văn B", 13));
        stds.display();
        
        PersonModel<Employee> emp = new PersonModel<>();
        emp.addObject(new Employee(1, "Nguyễn Văn A", 12));
        emp.addObject(new Employee(2, "Trần Văn B", 13));
        emp.display();
        
        PersonModel<String> stdName = new PersonModel<>();
        stdName.addObject("Trần Thị D");
        stdName.addObject("Lê Thị C");
        stdName.display();
    }
    
}
